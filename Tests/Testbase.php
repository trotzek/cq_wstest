<?php
namespace CITEQ\CqWstest\Tests;

use CITEQ\CqWstest\Tests\DataHandling\Assertion;
use CITEQ\CqWstest\Tests\DataHandling\Handler;
use CITEQ\CqWstest\Tests\DataHandling\WorkspaceHandler;
use TYPO3\CMS\Core\Utility\ExtensionManagementUtility;
use TYPO3\CMS\Core\Utility\GeneralUtility;

class Testbase extends \Tx_Phpunit_Database_TestCase {
    /**
     * @var /string
     */
    protected $path = NULL;

    /**
     * @var Handler
     */
    protected $dataHandler = NULL;

    /**
     * @var WorkspaceHandler
     */
    protected $workspaceHandler = NULL;
    /**
     * @var Assertion
     */
    protected $workspaceAssertion = NULL;

    /**
     * Some initialization stuff
     */
    public function setUp(){
        $this->initializeDatabase();
        $this->initializeObjects();
        $this->workspaceHandler->setWorkspace(0);
	}

    /**
     * Some cleanup stuff
     */
    public function tearDown(){
        $this->workspaceHandler->setWorkspace(0);
        $this->dropDatabase();
    }

	/**
	 * Initializes a test database.
	 *
	 * @return resource
	 */
	protected function initializeDatabase() {
        $importIfLoaded = array("cms", "workspaces", "version", "realurl", "cq_base");
		$hasDatabase = $this->createAndUseDatabase();
		if ($hasDatabase) {
			$this->importStdDB();
            foreach ($importIfLoaded as $extToImport){
                if (ExtensionManagementUtility::isLoaded($extToImport)){
                    $this->importExtensions(array($extToImport));
                }
            }

            //import fixtures
			$this->importDataSet($this->getPath().'Fixtures/pages.xml');
            $this->importDataSet($this->getPath().'Fixtures/sys_workspace.xml');
            $this->importDataSet($this->getPath().'Fixtures/sys_workspace_stage.xml');

            //copy current be_user to test db because otherwise there some errors during the workspace switch
            $beUser = array(
                "username" => $GLOBALS['BE_USER']->user['username'],
                "admin" => 1,
                "pid" => $GLOBALS['BE_USER']->user['pid'],
                "uid" => $GLOBALS['BE_USER']->user['uid']
            );
            $GLOBALS['TYPO3_DB']->exec_INSERTquery("be_users", $beUser);
		}
		return $hasDatabase;
	}

    protected function initializeObjects(){
        $this->dataHandler = new Handler();
        $this->workspaceAssertion = new Assertion();
        $this->workspaceHandler = new WorkspaceHandler();
    }

    /**
     * Returns the resource link to the test database (creates db if not exists)
     * @return resource
     */
    protected function createAndUseDatabase(){
        $hasDatabase = $this->createDatabase();

        if ($hasDatabase === TRUE) {
            $this->useTestDatabase();
        } else {
            $this->fail('No test database available');
        }

        return $hasDatabase;
    }

    /**
     * Returns the path of the cq_wstest extension
     * @return string
     */
    protected function getPath(){
		if (!isset($this->path)) {
			$this->path = ExtensionManagementUtility::extPath("cq_wstest")."Tests/";
		}
		return $this->path;
    }
}