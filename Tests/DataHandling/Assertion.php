<?php
namespace CITEQ\CqWstest\Tests\DataHandling;

use CITEQ\CqWstest\Tests\DataHandling\Handler;

class Assertion extends \Tx_Phpunit_Database_TestCase {
    /**
     * @var Handler
     */
    protected $dataHandler = NULL;

    public function __construct(){
        $this->dataHandler = new Handler();
    }

    /**
     * Checks if a the given data Record is a Live Record
     * @param Data $theData
     * @param string $message
     */
    public function assertIsLiveRecord (Data $theData, $message = NULL){
        $assertion = TRUE;
        if ($theData->get('pid') < 1) $assertion = FALSE;
        if ($theData->get('t3ver_oid') != 0) $assertion = FALSE;
        if ($theData->get('t3ver_wsid') != 0) $assertion = FALSE;
        if ($theData->get('t3ver_move_id') != 0) $assertion = FALSE;
        if ($theData->get('t3ver_state') != 0) $assertion = FALSE;
        if ($theData->get('t3ver_stage') != 0) $assertion = FALSE;
        $this->assertTrue($assertion, ($message === NULL) ? "Record is not a Live-Record" : $message);
    }

    /**
     * Checks if the given data Record is a Workspace Record
     * @param Data $theData
     * @param string $message
     */
    public function assertIsWorkspaceRecord(Data $theData, $message = NULL){
        $assertion = TRUE;
        if ($theData->get('pid') != -1) $assertion = FALSE;
        if ($theData->get('t3ver_wsid') < 1) $assertion = FALSE;
        $this->assertTrue($assertion, ($message === NULL) ? "Record is not a Workspace-Record" : $message);
    }

    /**
     * @param Data $liveRecord
     * @param Data $workspaceRecord
     * @param string $message
     */
    public function assertRecordIsWorkspaceVersionOfRecord(Data $liveRecord, Data $workspaceRecord, $message = NULL){
        $assertion = TRUE;

        if ($workspaceRecord->get('pid') != -1) $assertion = FALSE;
        if ($workspaceRecord->get('t3ver_id') < 1) $assertion = FALSE;
        if ($workspaceRecord->get('t3ver_wsid') < 1) $assertion = FALSE;
        if ($workspaceRecord->get('t3ver_oid') != $liveRecord->get('uid')) $assertion = FALSE;
        $this->assertTrue($assertion, ($message === NULL) ? "workspace Record is not a workspace version of the live record" : $message);
    }

    /**
     * @param Data $workspaceRecord
     * @param string $message
     */
    public function assertWorkspaceRecordHasNewPlaceholderInLive(Data $workspaceRecord, $message = NULL){
        $newPlaceholder = $this->dataHandler->getNewLivePlaceholderFor($workspaceRecord);
        $assertion = ($newPlaceholder !== NULL) ? TRUE : FALSE;
        $this->assertTrue($assertion, ($message === NULL) ? "No New Placeholder found for workspace Record" : $message);
    }

    /**
     * @param Data $workspaceRecord
     * @param string $message
     */
    public function assertWorkspaceRecordHasMovePlaceholder(Data $workspaceRecord, $message = NULL){
        $movePlaceholder = $this->dataHandler->getMovePlaceholderFor($workspaceRecord);
        $assertion = ($movePlaceholder!== NULL) ? TRUE : FALSE;
        $this->assertTrue($assertion, ($message === NULL) ? "No Move Placeholder found for workspace Record" : $message);
    }
}