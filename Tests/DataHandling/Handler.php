<?php
namespace CITEQ\CqWstest\Tests\DataHandling;

use TYPO3\CMS\Core\DataHandling\DataHandler;
use TYPO3\CMS\Core\Utility\DebugUtility;

Class Handler {
    /**
     * @var \TYPO3\CMS\Core\DataHandling\DataHandler
     */
    protected $theDataHandler = NULL;

    public function __construct(){
        $this->theDataHandler = new DataHandler();
    }

    /**
     * @param $dataToCreate Data
     * @return \CITEQ\CqWstest\Tests\DataHandling\Data
     */
    public function create(Data $dataToCreate){
        $cmd = array();
        $cmd[$dataToCreate->getTablename()]['NEW'] = array_merge($dataToCreate->getValues(), array("CType" => $dataToCreate->getCType()));
        $this->theDataHandler->start($cmd, array());
        $this->theDataHandler->process_datamap();

        if ($GLOBALS['BE_USER']->workspace !== 0){
            $placeholderUid = $this->theDataHandler->substNEWwithIDs['NEW'];
            $versionUid = $this->theDataHandler->autoVersionIdMap[$dataToCreate->getTablename()][$placeholderUid];
            $recordFromDb = $this->get($versionUid, $dataToCreate->getTablename());
        }else{
            $newUid = $this->theDataHandler->substNEWwithIDs['NEW'];
            $recordFromDb = $this->get($newUid , $dataToCreate->getTablename());
        }
        return $recordFromDb;
    }

    /**
     * @param $theModification Data
     * @param $dataToModify Data
     * @return \CITEQ\CqWstest\Tests\DataHandling\Data
     */
    public function modify(Data $theModification, Data $dataToModify){
        $cmd = array();
        $cmd[$dataToModify->getTablename()][$dataToModify->getUid()] = $theModification->getValues();
        $this->theDataHandler->start($cmd, array());
        $this->theDataHandler->process_datamap();

        if ($GLOBALS['BE_USER']->workspace !== 0){
            $versionUid = $this->theDataHandler->autoVersionIdMap[$dataToModify->getTablename()][$dataToModify->getUid()];
            $recordFromDb = $this->get($versionUid, $dataToModify ->getTablename());
        }else{
            $recordFromDb = $this->get($dataToModify->getUid(), $dataToModify->getTablename());
        }

        return $recordFromDb;
    }

    /**
     * Moves an element to another page
     * @param $recordToMove Data
     * @param $newPageUid
     * @return \CITEQ\CqWstest\Tests\DataHandling\Data
     */
    public function moveToPage(Data $recordToMove, $newPageUid){
        $cmd = array();
        $cmd[$recordToMove->getTablename()][$recordToMove->getUid()]['move'] = $newPageUid;
        $this->theDataHandler->start(array(), $cmd);
        $this->theDataHandler->process_cmdmap();

        if ($GLOBALS['BE_USER']->workspace !== 0){
            $versionUid = $this->theDataHandler->copyMappingArray_merged[$recordToMove->getTablename()][$recordToMove->getUid()];
            if ($versionUid === NULL){
                $recordFromDb = $this->get($recordToMove->getUid(), $recordToMove->getTablename());
            }else{
                $recordFromDb = $this->get($versionUid, $recordToMove ->getTablename());
            }
        }else{
            $recordFromDb = $this->get($recordToMove->getUid(), $recordToMove->getTablename());
        }

        return $recordFromDb;
    }

    /**
     * Localizes an Element and returns the localized record
     * @param \CITEQ\CqWstest\Tests\DataHandling\Data|\CITEQ\CqWstest\Tests\DataHandling\DataBase $recordToLocalize Data The record to localize
     * @param $languageUid int the uid to translate the given record into
     * @return Data the localized record
     */
    public function localize(DataBase $recordToLocalize, $languageUid){
        $cmd = array();
        $cmd[$recordToLocalize->getTablename()][$recordToLocalize->getUid()]['localize'] = $languageUid;
        $this->theDataHandler->start(array(), $cmd);
        $this->theDataHandler->process_cmdmap();

        if ($GLOBALS['BE_USER']->workspace !== 0){
            $versionCopyUid = $this->theDataHandler->copyMappingArray_merged[$recordToLocalize->getTablename()][$recordToLocalize->getUid()];
            $versionUid = $this->theDataHandler->autoVersionIdMap[$recordToLocalize->getTablename()][$versionCopyUid];
            $localizedFromDb = $this->get($versionUid, $recordToLocalize ->getTablename());
        }else{
            $localizedUid = $this->theDataHandler->copyMappingArray_merged[$recordToLocalize->getTablename()][$recordToLocalize->getUid()];
            $localizedFromDb = $this->get($localizedUid, $recordToLocalize->getTablename());
        }
        return $localizedFromDb;
    }

    /**
     * @param $uid int
     * @param $tableName string
     * @return \CITEQ\CqWstest\Tests\DataHandling\Data
     */
    public function get($uid, $tableName){
        $uid = intval($uid);
        $row = $GLOBALS['TYPO3_DB']->exec_SELECTgetSingleRow("*", $tableName, " uid = ".$uid);
        if (!is_array($row)){
            return NULL;
        }
        $data = new Data($tableName);
        $data->setValues($row);
        return $data;
    }

    /**
     * Refreshed multiple objects with the current values from the db by calling refreshRecordFromDb for each object
     * @param $recordsToRefresh array
     */
    public function refreshMultipleRecordsFromDb(&$recordsToRefresh){
        for ($i = 0; $i < count($recordsToRefresh); $i++){
            /** @var $this \CITEQ\CqWstest\Tests\DataHandling\Data */
            $this->refreshRecordFromDb($recordsToRefresh[$i]);
        }
    }

    /**
     * Refreshes the object with the current values from db
     * @param \CITEQ\CqWstest\Tests\DataHandling\Data|\CITEQ\CqWstest\Tests\DataHandling\DataBase $recordToRefresh
     */
    public function refreshRecordFromDb(DataBase &$recordToRefresh){
        $recordToRefresh = $this->get($recordToRefresh->getUid(), $recordToRefresh->getTablename());
    }

    /**
     * Gets a placeholder for the given data object
     * @param Data $data
     * @return \CITEQ\CqWstest\Tests\DataHandling\Data
     */
    public function getNewLivePlaceholderFor(Data $data){
        $row = $GLOBALS['TYPO3_DB']->exec_SELECTgetSingleRow("*", //fields
            $data->getTablename(), //table
            " uid = ".$data->get('t3ver_oid')."
              AND t3ver_state in (1,2)
              AND t3ver_wsid <> 0
              AND pid <> -1
              "); //where

        if (!is_array($row)){
             return NULL;
        }

        $placeHolder = new DataPlaceholder($data->getTablename());
        $placeHolder->setValues($row);
        $placeHolder->setPlaceholderFor($data);
        return $placeHolder;
    }

    /**
     * Gets the move Live-Placeholder for the given workspace record
     * @param Data $workspaceRecord
     * @return \CITEQ\CqWstest\Tests\DataHandling\DataPlaceholder|null
     */
    public function getMovePlaceholderFor(Data $workspaceRecord){
        //get the live record first
        $whereLiveRecord = "uid = ".$workspaceRecord->get('t3ver_oid') ." AND pid <> -1";
        $liveRow = $GLOBALS['TYPO3_DB']->exec_SELECTgetSingleRow("*", $workspaceRecord->getTablename(), $whereLiveRecord);

        if (!is_array($liveRow)){
            //move placeholder cannot be found
            return NULL;
        }

        //get the placeholder
        $whereMovePlaceholder = "t3ver_move_id = ".$liveRow['uid']."
                                  AND t3ver_state = 3
                                  AND t3ver_wsid <> 0
                                  AND pid <> -1";
        $movePlaceholderRow = $GLOBALS['TYPO3_DB']->exec_SELECTgetSingleRow("*", $workspaceRecord->getTablename(), $whereMovePlaceholder);

        if (!is_array($movePlaceholderRow)){
            //move placeholder cannot be found
            return NULL;
        }

        $placeHolder = new DataPlaceholder($workspaceRecord->getTablename());
        $placeHolder->setValues($movePlaceholderRow);
        $placeHolder->setPlaceholderFor($workspaceRecord);
        return $placeHolder;
    }
}