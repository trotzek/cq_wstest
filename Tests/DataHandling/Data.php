<?php
namespace CITEQ\CqWstest\Tests\DataHandling;

use CITEQ\CqWstest\Tests\DataHandling\DataBase;

class Data extends DataBase{
    public function __construct($tableName, $CType = NULL){
        parent::__construct($tableName, $CType);
    }
}