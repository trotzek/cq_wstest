<?php
namespace CITEQ\CqWstest\Tests\DataHandling;

use CITEQ\CqWstest\Tests\DataHandling\Data;
use CITEQ\CqWstest\Tests\DataHandling\DataBase;

Class DataPlaceholder extends DataBase {
    /**
     * @var bool
     */
    protected $isNewPlaceholder = TRUE;

    /**
     * @var bool
     */
    protected $isMovePlaceholder = FALSE;

    /**
     * @var Data
     */
    protected $placeholderFor = NULL;

    /**
     * @param $tableName
     * @param null $CType
     */
    public function __construct($tableName, $CType = NULL){
        parent::__construct($tableName, $CType);
    }

    /**
     * @param boolean $isMovePlaceholder
     */
    public function setIsMovePlaceholder($isMovePlaceholder)
    {
        $this->isMovePlaceholder = $isMovePlaceholder;
    }

    /**
     * @return boolean
     */
    public function getIsMovePlaceholder()
    {
        return $this->isMovePlaceholder;
    }

    /**
     * @return \CITEQ\CqWstest\Tests\DataHandling\Data
     */
    public function getPlaceholderFor()
    {
        return $this->placeholderFor;
    }

    /**
     * @param \CITEQ\CqWstest\Tests\DataHandling\Data $placeholderFor
     */
    public function setPlaceholderFor($placeholderFor)
    {
        $this->placeholderFor = $placeholderFor;
    }

    /**
     * @return boolean
     */
    public function getIsNewPlaceholder()
    {
        return $this->isNewPlaceholder;
    }

    /**
     * @param boolean $isNewPlaceholder
     */
    public function setIsNewPlaceholder($isNewPlaceholder)
    {
        $this->isNewPlaceholder = $isNewPlaceholder;
    }
}