<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Trotzek
 * Date: 29.07.13
 * Time: 13:18
 * To change this template use File | Settings | File Templates.
 */

namespace CITEQ\CqWstest\Tests\DataHandling;

use CITEQ\CqWstest\Tests\DataHandling\Data;
use TYPO3\CMS\Backend\Utility\BackendUtility;
use TYPO3\CMS\Core\Authentication\BackendUserAuthentication;
use TYPO3\CMS\Core\Utility\ExtensionManagementUtility;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Workspaces\ExtDirect\ActionHandler;

class WorkspaceHandler {

    /**
     * @var ActionHandler
     */
    protected $workspaceActionHandler = NULL;

    public function __construct(){
        $this->workspaceActionHandler = new ActionHandler();
    }

    /**
     * Sets the workspace to the given uid
     * @param $workspaceUid int
     */
    public function setWorkspace($workspaceUid){
        $GLOBALS['BE_USER']->setWorkspace($workspaceUid);
    }

    /**
     * Publishes Data to LIVE
     * @param Data $workspaceRecordToPublish
     * @param int $liveUid
     */
    public function publish($workspaceRecordToPublish, $liveUid){
        $arguments = new \stdClass();
        $arguments->affects->nextStage = -20;
        $arguments->affects->table = $workspaceRecordToPublish->getTablename();
        $arguments->affects->uid = $workspaceRecordToPublish->getUid();
        $arguments->affects->t3ver_oid = $liveUid;
        $arguments->receipients = array();
        $arguments->additional = '';
        $arguments->comments = '';
        $this->workspaceActionHandler->sendToNextStageExecute($arguments);
    }
}