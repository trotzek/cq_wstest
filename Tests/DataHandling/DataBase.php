<?php
namespace CITEQ\CqWstest\Tests\DataHandling;

class DataBase {
    /**
     * @var int
     */
    protected $uid = NULL;
    /**
     * @var string
     */
    protected $tableName = NULL;
    /**
     * @var string
     */
    protected $CType = "text";

    /**
     * @var array
     */
    protected $keyValues = array();


    protected function __construct($tableName, $CType = NULL){
        $this->tableName = $tableName;

        if ($CType !== NULL){
            $this->CType = $CType;
        }
    }

    /**
     * Sets the value for the given key
     * @param $key string
     * @param $value mixed
     */
    public function set($key, $value){
        if ($key === "uid"){
            $this->uid = $value;
        }
        $this->keyValues[$key] = $value;
    }

    /**
     * Returns the tablename
     * @return string
     */
    public function getTablename(){
        return $this->tableName;
    }

    /**
     * Gets the CType of the record
     * @return null|string
     */
    public function getCType(){
        return $this->CType;
    }

    /**
     * Sets the uid of the record
     * @param $int
     */
    public function setUid($int){
        $this->uid = $int;
    }

    /**
     * Gets the uid id of the data record
     * @return int|null
     */
    public function getUid(){
        return $this->uid;
    }

    /**
     * Sets the given values
     * @param $values array
     */
    public function setValues($values){
        foreach ($values as $key => $value){
            $this->set($key, $value);
        }
    }

    /**
     * Returns all values as array
     * @return array
     */
    public function getValues(){
        return $this->keyValues;
    }

    /**
     * Returns the value for a specific key
     * @param $key string
     * @return mixed
     */
    public function get($key){
        return $this->keyValues[$key];
    }
}