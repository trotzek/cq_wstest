<?php
namespace CITEQ\CqWstest\Tests\BasicTests;
use CITEQ\CqWstest\Tests\DataHandling\Data;
use CITEQ\CqWstest\Tests\Testbase;
use TYPO3\CMS\Core\Utility\GeneralUtility;

/**
 * @author Uwe Trotzek <uwe.trotzek@googlemail.com>
 */
class SimpleCeTest extends Testbase{
    /**
     * @test
     * @return void
     */
    public function createSingleElementCreatesElement(){
        //create element
        $contentElement = new Data('tt_content');
        $contentElement->set('pid', 1);
        $contentElement->set('header', 'this is a single element');
        $liveRecord = $this->dataHandler->create($contentElement);

        //assertion
        $this->workspaceAssertion->assertIsLiveRecord($liveRecord);
    }

    /**
     * @test
     * @return void
     */
    public function modifyElementInWorkspaceCreatesWorkspaceRecord(){
        //create element
        $createElement = new Data('tt_content');
        $createElement->set('pid', 1);
        $createElement->set('header', 'this is the initial value');
        $liveRecord = $this->dataHandler->create($createElement);

        //switch to workspace 1
        $this->workspaceHandler->setWorkspace(1);

        //modify Element
        $modification = new Data('tt_content');
        $modification->set('header', 'this value has been modified');
        $workspaceRecord = $this->dataHandler->modify($modification, $liveRecord);

        //assertions
        $this->workspaceAssertion->assertIsWorkspaceRecord($workspaceRecord);
        $this->workspaceAssertion->assertRecordIsWorkspaceVersionOfRecord($liveRecord, $workspaceRecord);
    }

    /**
     * @test
     * @return void
     */
    public function modifyAndPublishExistingElementWorks(){
        //create element
        $contentElement = new Data('tt_content');
        $contentElement->set('pid', 1);
        $contentElement->set('header', 'this is the initial value');
        $liveRecord = $this->dataHandler->create($contentElement);

        //switch to workspace 1
        $this->workspaceHandler->setWorkspace(1);

        //modify element
        $modification = new Data('tt_content');
        $modification ->set('header', 'this value has been modified');
        $workspaceRecord = $this->dataHandler->modify($modification, $liveRecord);

        //publish element
        $this->workspaceHandler->publish($workspaceRecord, $liveRecord->getUid());

        //refresh the liveRecord Values to be able to check if they are correct in the next step
        $this->dataHandler->refreshRecordFromDb($liveRecord);

        //assertions
        $this->workspaceAssertion->assertIsLiveRecord($liveRecord);
        $this->assertEquals('this value has been modified', $liveRecord->get('header'), 'live record hasn\'t been updated');
    }

    /**
     * @test
     */
    public function createdElementInWorkspaceGeneratesNewPlaceholder(){
        //switch to workspace 1
        $this->workspaceHandler->setWorkspace(1);

        //create element
        $createElement = new Data('tt_content');
        $createElement->set('pid', 1);
        $createElement->set('header', 'this element was created in a workspace');
        $workspaceRecord = $this->dataHandler->create($createElement);

        //assertion
        $this->workspaceAssertion->assertWorkspaceRecordHasNewPlaceholderInLive($workspaceRecord);
    }

    /**
     * @test
     * @return void
     */
    public function createdElementInWorkspaceReplacesNewPlaceholderAfterPublish(){
        //switch to workspace 1
        $this->workspaceHandler->setWorkspace(1);

        //create element
        $createElement = new Data('tt_content');
        $createElement->set('pid', 1);
        $createElement->set('header', 'this element was created in a workspace');
        $createElement->set('bodytext', 'this is a bodytext');
        $workspaceRecord = $this->dataHandler->create($createElement);
        $newPlaceHolder = $this->dataHandler->getNewLivePlaceholderFor($workspaceRecord);

        //publish
        $this->workspaceHandler->publish($workspaceRecord, $newPlaceHolder->getUid());

        //assertion
        $this->dataHandler->refreshRecordFromDb($newPlaceHolder);
        $this->assertEquals('this is a bodytext', $newPlaceHolder->get('bodytext'));
    }

    /**
     * @test
     */
    public function movedLiveRecordInWorkspaceCreatesMovePlaceholder(){
        //create element
        $createElement = new Data('tt_content');
        $createElement->set('pid', 1);
        $createElement->set('header', 'this element was created in a workspace');
        $createElement->set('bodytext', 'this is a bodytext');
        $liveRecord = $this->dataHandler->create($createElement);

        //switch to workspace 1
        $this->workspaceHandler->setWorkspace(1);

        //move to page 2
        $workspaceRecord = $this->dataHandler->moveToPage($liveRecord, 2);

        //assertions
        $movePlaceholder = $this->dataHandler->getMovePlaceholderFor($workspaceRecord);
        $this->workspaceAssertion->assertWorkspaceRecordHasMovePlaceholder($workspaceRecord);
        $this->assertEquals(2, $movePlaceholder->get('pid'), 'move placeholder hasn\t the correct pid');
    }

    /**
     * @test
     */
    public function movedAndPublishedLiveRecordGetsNewPid(){
        //create element
        $createElement = new Data('tt_content');
        $createElement->set('pid', 1);
        $createElement->set('header', 'this element was created in a workspace');
        $createElement->set('bodytext', 'this is a bodytext');
        $liveRecord = $this->dataHandler->create($createElement);

        //modify
        $this->workspaceHandler->setWorkspace(1);
        $workspaceRecord = $this->dataHandler->moveToPage($liveRecord, 2);

        //publish
        $this->workspaceHandler->publish($workspaceRecord,$liveRecord->getUid());

        //assertions
        $this->dataHandler->refreshRecordFromDb($liveRecord);
        $movePlaceholder = $this->dataHandler->getMovePlaceholderFor($workspaceRecord);
        $this->assertEquals(2, $liveRecord->get('pid'));
        $this->assertNull($movePlaceholder, 'move placeholder was not removed correctly');
    }

    /**
     * @test
     */
    public function movedAndPublishedWorkspaceRecordReplacesPlaceholderCorrectly(){
        //switch to workspace 1
        $this->workspaceHandler->setWorkspace(1);

        //create element
        $createElement = new Data('tt_content');
        $createElement->set('pid', 1);
        $createElement->set('header', 'this element was created in a workspace');
        $createElement->set('bodytext', 'this is a bodytext');
        $workspaceRecord = $this->dataHandler->create($createElement);

        //modify
        $workspaceRecord = $this->dataHandler->moveToPage($workspaceRecord, 2);
        $newPlaceHolder = $this->dataHandler->getNewLivePlaceholderFor($workspaceRecord);

        //publish
        $this->workspaceHandler->publish($workspaceRecord,$newPlaceHolder->getUid());
        $this->dataHandler->refreshRecordFromDb($newPlaceHolder);

        //assertions
        $this->assertEquals(2, $newPlaceHolder->get('pid'));
        $this->assertEquals('this is a bodytext', $newPlaceHolder->get('bodytext'));
    }
}
// upcoming testcases...
//movedPublishedChangedPublishedLiveRecordInWorkspaceUpdatesLiveRecordCorrectly
//movedPublishedChangedPublishedWorkspaceRecordCreatesLiveRecordCorrectly
//delete*
//copy*