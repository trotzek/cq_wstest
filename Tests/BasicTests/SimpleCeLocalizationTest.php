<?php
namespace CITEQ\CqWstest\Tests\BasicTests;
use CITEQ\CqWstest\Tests\DataHandling\Data;
use CITEQ\CqWstest\Tests\Testbase;
use TYPO3\CMS\Core\Utility\GeneralUtility;

/**
 * @author Uwe Trotzek <uwe.trotzek@googlemail.com>
 */
class SimpleCeLocalizationTest extends Testbase{
    const LANGUAGE_SPANISH = 1;
    const LANGUAGE_FRENCH = 2;

    const LANGUAGE_SPANISH_BODYTEXT = 'Este es el contenido de mi';
    const LANGUAGE_FRENCH_BODYTEXT = 'c\'est mon contenu';
    /**
     * Load additional language settings
     * @return void
     */
    public function setUp(){
        parent::setUp();
        $this->importDataSet($this->getPath().'Fixtures/sys_language.xml');
        $this->importDataSet($this->getPath().'Fixtures/pages_language_overlay.xml');
    }

    /**
     * @test
     */
    public function localizeExistingElementsGeneratesLocalizedElements(){
        //create element
        $contentElement = new Data('tt_content');
        $contentElement->set('pid', 1);
        $contentElement->set('header', 'this is a single element');
        $liveElement = $this->dataHandler->create($contentElement);

        //localize
        $spanishRecord = $this->dataHandler->localize($liveElement,self::LANGUAGE_SPANISH);
        $frenchRecord = $this->dataHandler->localize($liveElement, self::LANGUAGE_FRENCH);

        //assertion
        $this->assertEquals(self::LANGUAGE_SPANISH, $spanishRecord->get('sys_language_uid'), "spanish record could not be generated correctly");
        $this->assertEquals(self::LANGUAGE_FRENCH, $frenchRecord->get('sys_language_uid'), "french record could not be generated correctly");
    }

    /**
     * @test
     */
    public function localizeLiveRecordsInWorkspaceGeneratesPlaceholderInLiveMode(){
        //create element
        $contentElement = new Data('tt_content');
        $contentElement->set('pid', 1);
        $contentElement->set('header', 'this is a single element');
        $liveElement = $this->dataHandler->create($contentElement);

        //switch to worksapce 1
        $this->workspaceHandler->setWorkspace(1);

        //localize
        $spanishWsRecord = $this->dataHandler->localize($liveElement,self::LANGUAGE_SPANISH);
        $frenchWsRecord = $this->dataHandler->localize($liveElement, self::LANGUAGE_FRENCH);

        //assertions
        $this->workspaceAssertion->assertWorkspaceRecordHasNewPlaceholderInLive($spanishWsRecord);
        $this->workspaceAssertion->assertWorkspaceRecordHasNewPlaceholderInLive($frenchWsRecord);
    }

    /**
     * @test
     */
    public function localizedAndMovedLiveRecordInWorkspaceUpdatesNewPlaceholder(){
    //create element
    $contentElement = new Data('tt_content');
    $contentElement->set('pid', 1);
    $contentElement->set('header', 'this is a single element');
    $liveElement = $this->dataHandler->create($contentElement);

    //switch to worksapce 1
    $this->workspaceHandler->setWorkspace(1);

    //localize
    $spanishWsRecord = $this->dataHandler->localize($liveElement,self::LANGUAGE_SPANISH);
    $frenchWsRecord = $this->dataHandler->localize($liveElement, self::LANGUAGE_FRENCH);

    $spanishNewPlaceholder = $this->dataHandler->getNewLivePlaceholderFor($spanishWsRecord);
    $frenchNewPlacerholder = $this->dataHandler->getNewLivePlaceholderFor($frenchWsRecord);

    //move to new page
    $this->dataHandler->moveToPage($spanishWsRecord, 2);
    $this->dataHandler->moveToPage($frenchWsRecord, 3);

    $this->dataHandler->refreshRecordFromDb($spanishNewPlaceholder);
    $this->dataHandler->refreshRecordFromDb($frenchNewPlacerholder);

    //assertions
    $this->assertEquals(2, $spanishNewPlaceholder->get('pid'));
    $this->assertEquals(3, $frenchNewPlacerholder->get('pid'));
}

    /**
     * @test
     */
    public function localizedMovedAndPublishedLiveRecordInWorkspaceRemovesNewPlaceholder(){
        //create element
        $contentElement = new Data('tt_content');
        $contentElement->set('pid', 1);
        $contentElement->set('header', 'this is a single element');
        $liveElement = $this->dataHandler->create($contentElement);

        //switch to worksapce 1
        $this->workspaceHandler->setWorkspace(1);

        //localize
        $spanishWsRecord = $this->dataHandler->localize($liveElement,self::LANGUAGE_SPANISH);
        $frenchWsRecord = $this->dataHandler->localize($liveElement, self::LANGUAGE_FRENCH);

        $spanishNewPlaceholder = $this->dataHandler->getNewLivePlaceholderFor($spanishWsRecord);
        $frenchNewPlacerholder = $this->dataHandler->getNewLivePlaceholderFor($frenchWsRecord);

        //move to new page
        $this->dataHandler->moveToPage($spanishWsRecord, 2);
        $this->dataHandler->moveToPage($frenchWsRecord, 3);

        //publish
        $this->workspaceHandler->publish($spanishWsRecord, $spanishNewPlaceholder->getUid());
        $this->workspaceHandler->publish($frenchWsRecord, $frenchNewPlacerholder->getUid());

        //assertions
        $this->dataHandler->refreshRecordFromDb($spanishNewPlaceholder);
        $this->dataHandler->refreshRecordFromDb($frenchNewPlacerholder);
        $this->dataHandler->refreshRecordFromDb($spanishWsRecord);
        $this->dataHandler->refreshRecordFromDb($frenchWsRecord);
        $this->assertEquals(2, $spanishNewPlaceholder->get('pid'));
        $this->assertEquals(3, $frenchNewPlacerholder->get('pid'));
        $this->assertNull($spanishWsRecord, 'spanish workspace record has not been removed after publish');
        $this->assertNull($frenchWsRecord, 'french workspace record has not been removed after publish');
    }

    /**
     * @test
     */
    public function localizedModifiedAndPublishedLiveRecordUpdatesNewPlaceholder(){
        //create element
        $contentElement = new Data('tt_content');
        $contentElement->set('pid', 1);
        $contentElement->set('header', 'this is a single element');
        $contentElement->set('bodytext', 'this is my content');
        $liveElement = $this->dataHandler->create($contentElement);

        //switch to worksapce 1
        $this->workspaceHandler->setWorkspace(1);

        //localize
        $spanishWsRecord = $this->dataHandler->localize($liveElement,self::LANGUAGE_SPANISH);
        $frenchWsRecord = $this->dataHandler->localize($liveElement, self::LANGUAGE_FRENCH);
        $spanishNewPlaceholder = $this->dataHandler->getNewLivePlaceholderFor($spanishWsRecord);
        $frenchNewPlacerholder = $this->dataHandler->getNewLivePlaceholderFor($frenchWsRecord);

        //modify
        $spanishModification = new Data('tt_content');
        $spanishModification->set('bodytext', self::LANGUAGE_SPANISH_BODYTEXT);
        $this->dataHandler->modify($spanishModification, $spanishWsRecord);

        $frenchModification = new Data('tt_content');
        $frenchModification->set('bodytext', self::LANGUAGE_FRENCH_BODYTEXT);
        $this->dataHandler->modify($frenchModification, $frenchWsRecord);

        //publish
        $this->workspaceHandler->publish($spanishWsRecord, $spanishNewPlaceholder->getUid());
        $this->workspaceHandler->publish($frenchWsRecord, $frenchNewPlacerholder->getUid());

        //assertions
        $this->dataHandler->refreshRecordFromDb($spanishNewPlaceholder);
        $this->dataHandler->refreshRecordFromDb($frenchNewPlacerholder);
        $this->assertEquals(self::LANGUAGE_SPANISH_BODYTEXT, $spanishNewPlaceholder->get('bodytext'));
        $this->assertEquals(self::LANGUAGE_FRENCH_BODYTEXT, $frenchNewPlacerholder->get('bodytext'));
    }

    /**
     * @test
     */
    public function createdAndLocalizedElementInWorkspaceCreatesPlaceholder(){
        //switch to worksapce 1
        $this->workspaceHandler->setWorkspace(1);

        $contentElement = new Data('tt_content');
        $contentElement->set('pid', 1);
        $contentElement->set('header', 'this is a single element');
        $contentElement->set('bodytext', 'this is my content');
        $wsElement = $this->dataHandler->create($contentElement);
        //get new placeholder because only live records can be localized
        $elementNewPlaceholder = $this->dataHandler->getNewLivePlaceholderFor($wsElement);

        //localize
        $spanishWsRecord = $this->dataHandler->localize($elementNewPlaceholder,self::LANGUAGE_SPANISH);
        $frenchWsRecord = $this->dataHandler->localize($elementNewPlaceholder, self::LANGUAGE_FRENCH);

        //assertions
        $this->assertNotNull($spanishWsRecord, 'spanish workspace record was not generated correctly');
        $this->assertNotNull($frenchWsRecord, 'french workspace record was not generated correctly');
        $this->workspaceAssertion->assertWorkspaceRecordHasNewPlaceholderInLive($wsElement);
        $this->workspaceAssertion->assertWorkspaceRecordHasNewPlaceholderInLive($spanishWsRecord);
        $this->workspaceAssertion->assertWorkspaceRecordHasNewPlaceholderInLive($frenchWsRecord);
    }

    /**
     * @test
     */
    public function createdLocalizedAndPublishedWorkspaceElementUpdatesPlaceholderProperly(){
        //switch to worksapce 1
        $this->workspaceHandler->setWorkspace(1);

        $contentElement = new Data('tt_content');
        $contentElement->set('pid', 1);
        $contentElement->set('header', 'this is a single element');
        $contentElement->set('bodytext', 'this is my content');
        $wsElement = $this->dataHandler->create($contentElement);
        //get the new placeholder here because only live records can be localized
        $elementNewPlaceholder = $this->dataHandler->getNewLivePlaceholderFor($wsElement);

        //localize and modify
        $spanishWsRecord = $this->dataHandler->localize($elementNewPlaceholder,self::LANGUAGE_SPANISH);
        $spanishModification = new Data('tt_content');
        $spanishModification->set('bodytext', self::LANGUAGE_SPANISH_BODYTEXT);
        $this->dataHandler->modify($spanishModification, $spanishWsRecord);

        $frenchWsRecord = $this->dataHandler->localize($elementNewPlaceholder,self::LANGUAGE_FRENCH);
        $frenchModification = new Data('tt_content');
        $frenchModification->set('bodytext', self::LANGUAGE_FRENCH_BODYTEXT);
        $this->dataHandler->modify($frenchModification, $frenchWsRecord);

        $spanishNewPlaceholder = $this->dataHandler->getNewLivePlaceholderFor($spanishWsRecord);
        $frenchNewPlacerholder = $this->dataHandler->getNewLivePlaceholderFor($frenchWsRecord);

        //publish
        $this->workspaceHandler->publish($spanishWsRecord, $spanishNewPlaceholder->getUid());
        $this->workspaceHandler->publish($frenchWsRecord, $frenchNewPlacerholder->getUid());
        $this->dataHandler->refreshRecordFromDb($spanishNewPlaceholder);
        $this->dataHandler->refreshRecordFromDb($frenchNewPlacerholder);
        $this->dataHandler->refreshRecordFromDb($spanishWsRecord);
        $this->dataHandler->refreshRecordFromDb($frenchWsRecord);

        //assertions
        $this->assertEquals(self::LANGUAGE_SPANISH_BODYTEXT, $spanishNewPlaceholder->get('bodytext'), 'spanish new placeholder was not updated properly after publish');
        $this->assertEquals(self::LANGUAGE_FRENCH_BODYTEXT, $frenchNewPlacerholder->get('bodytext'), 'french new placeholder was not updated properly after publish');
        $this->assertNull($spanishWsRecord, 'spanish workspace record was not removed correctly after publish');
        $this->assertNull($frenchWsRecord, 'french workspace record was not removed correctly after publish');

    }
}