<?php
use TYPO3\CMS\Core\Utility\ExtensionManagementUtility;

$extensionPath = ExtensionManagementUtility::extPath("cq_wstest");
return array(
    'Testbase' => $extensionPath . 'Tests/Testbase.php',
    'Data' => $extensionPath . 'Tests/DataHandler/Data.php',
    'Handler' => $extensionPath . 'Tests/DataHandler/Handler.php',
    'AssertionHandler' => $extensionPath . 'Tests/DataHandler/AssertionHandler.php',
);